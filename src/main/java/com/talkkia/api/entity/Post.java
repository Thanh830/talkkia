package com.talkkia.api.entity;

import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.List;

@Node
public class Post {

    @Id
    private Long PostId;
    private String postText;
    private String[] postPicVid;
    private String dateTime;
    @Relationship
    private User postUser;
    @Relationship
    private List<User> likes;
    @Relationship
    private List<User> dislikes;
    @Relationship
    private List<Comment> comments;

}
