package com.talkkia.api.entity;

import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;

@Node
public class Comment {

    @Id
    private Long commentId;
    private String text;
    private String commentPicVid;
    private String timestamp;
    @Relationship
    private User user;

}
