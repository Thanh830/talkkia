package com.talkkia.api.dto;


import javax.validation.constraints.NotNull;
import java.util.List;

public class PostDTO {

    private Long PostId;
    @NotNull
    private String postText;
    private String[] postPicVid;
    @NotNull
    private String dateTime;
    @NotNull
    private UserDTO postUser;
    private List<UserDTO> likes;
    private List<UserDTO> dislikes;
    private List<CommentDTO> comments;

    public PostDTO() {
    }

    public PostDTO(Long postId, @NotNull String postText, String[] postPicVid, @NotNull String dateTime, @NotNull UserDTO postUser, List<UserDTO> likes, List<UserDTO> dislikes, List<CommentDTO> comments) {
        PostId = postId;
        this.postText = postText;
        this.postPicVid = postPicVid;
        this.dateTime = dateTime;
        this.postUser = postUser;
        this.likes = likes;
        this.dislikes = dislikes;
        this.comments = comments;
    }

    public Long getPostId() {
        return PostId;
    }

    public void setPostId(Long postId) {
        PostId = postId;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String[] getPostPicVid() {
        return postPicVid;
    }

    public void setPostPicVid(String[] postPicVid) {
        this.postPicVid = postPicVid;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public UserDTO getPostUser() {
        return postUser;
    }

    public void setPostUser(UserDTO postUser) {
        this.postUser = postUser;
    }

    public List<UserDTO> getLikes() {
        return likes;
    }

    public void setLikes(List<UserDTO> likes) {
        this.likes = likes;
    }

    public List<UserDTO> getDislikes() {
        return dislikes;
    }

    public void setDislikes(List<UserDTO> dislikes) {
        this.dislikes = dislikes;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }
}
