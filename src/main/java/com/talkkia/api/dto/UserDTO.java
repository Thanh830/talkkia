package com.talkkia.api.dto;

import org.springframework.data.annotation.Version;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {

    @Id
    private Long id;
    @Version
    private Long version;
    private String name;
    private String dob;
    private String gender;
    private String mobile;
    private String email;
    private String address;
    private String picture;
    private String registrationDate;
    @Relationship(type = "CONTACT")
    List<UserDTO> contacts = new ArrayList<>();

    public UserDTO(Long id, Long version, String name, String dob, String gender, String mobile, String email, String address, String picture, String registrationDate, List<UserDTO> contacts) {
        this.id = id;
        this.version = version;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.mobile = mobile;
        this.email = email;
        this.address = address;
        this.picture = picture;
        this.registrationDate = registrationDate;
        this.contacts = contacts;
    }

    public UserDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<UserDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<UserDTO> contacts) {
        this.contacts = contacts;
    }
}
