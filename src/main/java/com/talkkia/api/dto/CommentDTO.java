package com.talkkia.api.dto;

public class CommentDTO {

    private Long commentId;
    private String text;
    private String commentPicVid;
    private String timestamp;
    private UserDTO user;

    public CommentDTO() {
    }

    public CommentDTO(Long commentId, String text, String commentPicVid, String timestamp, UserDTO user) {
        this.commentId = commentId;
        this.text = text;
        this.commentPicVid = commentPicVid;
        this.timestamp = timestamp;
        this.user = user;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCommentPicVid() {
        return commentPicVid;
    }

    public void setCommentPicVid(String commentPicVid) {
        this.commentPicVid = commentPicVid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
