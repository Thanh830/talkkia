package com.talkkia.api.mapper;

import com.talkkia.api.dto.UserDTO;
import com.talkkia.api.entity.User;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

public interface UserMapper extends EntityMapper<UserDTO, User> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoId(User user);
}